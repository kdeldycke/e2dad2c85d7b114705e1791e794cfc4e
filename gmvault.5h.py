#!/usr/bin/env python
# -*- coding: utf-8 -*-
# <bitbar.title>Gmvault</bitbar.title>
# <bitbar.version>v1.0.0</bitbar.version>
# <bitbar.author>Kevin Deldycke</bitbar.author>
# <bitbar.author.github>kdeldycke</bitbar.author.github>
# <bitbar.desc>Run Gmvault Gmail backup regularly. You can install Gmvault with
# pip: $ pip install --upgrade gmvault</bitbar.desc>
# <bitbar.dependencies>python,gmvault</bitbar.dependencies>
# <bitbar.image></bitbar.image>

from __future__ import print_function, unicode_literals

import os
from os.path import isfile, join, expanduser
import subprocess
import re
import sys


def is_gmvault_running():
    """ Return True if a `gmvault sync` process is running. """
    process_list = subprocess.Popen(
        ['ps', '-caxwwww', '-ocomm=,args='],
        stdout=subprocess.PIPE).communicate()[0]
    for line in process_list.split('\n'):
        line = line.strip()
        if not line:
            continue
        result = re.split(r'\s+', line, maxsplit=1)
        if len(result) < 2:
            continue
        command, args = result
        if command == 'bash' and args.startswith('gmvault sync'):
            return True


def get_account_list():
    """ Return the list of registered accounts. """
    config_path = expanduser('~/.gmvault')
    accounts = []
    for filename in os.listdir(config_path):
        if isfile(join(config_path, filename)):
            if filename.endswith('.oauth2'):
                accounts.append(filename.split('.oauth2', 1)[0])
    return accounts


def print_menu():
    """ Print menu structure using BitBar's plugin API.

    See: https://github.com/matryer/bitbar#plugin-api
    """
    # Print menu bar icon.
    print(("📩" if is_gmvault_running() else "✉️").encode('utf-8'))

    print("---")
    # Allow sync of a single account.
    for account_id in get_account_list():
        print("Sync {} | terminal=true bash={} param1={}".format(
            account_id, __file__, account_id))

    print("---")
    print("Setup Gmvault | href=http://gmvault.org/gmail_setup.html#quickstart")


# Fetch the single parameter allowed if provided.
arg = sys.argv[1] if len(sys.argv) == 2 else None

# Called with no argument: print menu.
if not arg:
    print_menu()

# Called with an argument: force sync.
elif arg in get_account_list():
    subprocess.call(['gmvault', 'sync', sys.argv[1]])
